# ***********************************************
# ***          Loco363 - Panel - PHL          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# path to panel GIT root
rootPath = 5 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic

# import parts
from types import SimpleNamespace
parts = SimpleNamespace()

parts.BattMeter = load('parts.battmeter', rootPath + 'parts/battmeter/cad/drill.py', __file__).BattMeter
parts.Indicator = load('parts.indicator', rootPath + 'parts/indicator/cad/drill.py', __file__).Indicator
parts.Meter = load('parts.meter', rootPath + 'parts/meter/cad/drill.py', __file__).Meter
parts.TRS = load('parts.trs', rootPath + 'parts/trs/cad/drill.py', __file__).TRS


class PanelPHL(gen_draw.Drawing):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c)
		
		# make sure we pass a valid coordinates to our children nodes
		c = self.getCoords()
		
		# border
		self.add(generic.LabeledPolygon(
			c,
			[
				(   0,   0, (-2, -5, 'end')),
				(-650,   0),
				(-650, 250),
				(   0, 250, (-2, -5, 'end'))
			],
			{
				'fill': 'none',
				'stroke': 'black',
				'stroke-width': 3
			}
		))
		
		# mounting holes
		self.add(generic.DrillHole(C(c, (- 10,  10)), 4, 'Z', cpos=(-2, -5, 'end')))
		self.add(generic.DrillHole(C(c, (-325,  10)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (-640,  10)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (- 10, 240)), 4, 'Z', cpos=(-2, -5, 'end')))
		self.add(generic.DrillHole(C(c, (-285, 240)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (-640, 240)), 4, 'Z', pos=(-3, -2, 'end'), cpos=(2, 1)))
		
		# meters
		self.add(parts.Meter(C(c, (- 55,  68.75))))
		self.add(parts.Meter(C(c, (-130,  68.75))))
		self.add(parts.Meter(C(c, (- 55, 181.25))))
		self.add(parts.Meter(C(c, (-130, 181.25))))
		self.add(parts.Meter(C(c, (-205, 181.25))))
		
		# indicators
		self.add(parts.Indicator(C(c, (-210, 68.75))))
		self.add(parts.Indicator(C(c, (-285, 68.75))))
		self.add(parts.Indicator(C(c, (-360, 68.75))))
		self.add(parts.Indicator(C(c, (-450, 68.75))))
		
		# battery meters
		self.add(parts.BattMeter(C(c, (-534.5,  68.75))))
		self.add(parts.BattMeter(C(c, (-600.5,  68.75))))
		
		# radio-station
		self.add(parts.TRS(C(c, (-470, 175))))
	# constructor
# class PanelPHL


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPHL(), True))
