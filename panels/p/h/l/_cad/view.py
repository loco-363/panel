# ***********************************************
# ***          Loco363 - Panel - PHL          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(5*'../' + 'lib/cad', __file__)
import generic


class PanelPHL(generic.bases.ViewPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			[
				(  0,   0),
				(650,   0),
				(650, 250),
				(  0, 250)
			]
		)
	# constructor
# class PanelPHL


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPHL(), True))
