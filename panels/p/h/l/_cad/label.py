# ***********************************************
# ***          Loco363 - Panel - PHL          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# path to panel GIT root
rootPath = 5 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic

# import parts
from types import SimpleNamespace
parts = SimpleNamespace()

parts.BattMeter = load('parts.battmeter', rootPath + 'parts/battmeter/cad/label.py', __file__).BattMeter
parts.Indicator = load('parts.indicator', rootPath + 'parts/indicator/cad/label.py', __file__).Indicator
parts.Meter = load('parts.meter', rootPath + 'parts/meter/cad/label.py', __file__).Meter
parts.TRS = load('parts.trs', rootPath + 'parts/trs/cad/label.py', __file__).TRS


class PanelPHL(generic.bases.LabelPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			'L',
			[
				(  0,   0),
				(650,   0),
				(650, 250),
				(  0, 250)
			]
		)
		
		# move zero to (650, 0)
		c = C(self.getCoords(), (650, 0))
		
		# meters
		self.add(parts.Meter(C(c, (- 55,  68.75)), 'M5'))
		self.add(parts.Meter(C(c, (-130,  68.75)), 'M4'))
		self.add(parts.Meter(C(c, (- 55, 181.25)), 'M3'))
		self.add(parts.Meter(C(c, (-130, 181.25)), 'M2'))
		self.add(parts.Meter(C(c, (-205, 181.25)), 'M1'))
		
		# indicators
		self.add(parts.Indicator(C(c, (-210, 68.75)), 'U4'))
		self.add(parts.Indicator(C(c, (-285, 68.75)), 'U3'))
		self.add(parts.Indicator(C(c, (-360, 68.75)), 'U2'))
		self.add(parts.Indicator(C(c, (-450, 68.75)), 'U1'))
		
		# battery meters
		self.add(parts.BattMeter(C(c, (-534.5,  68.75)), 'BV'))
		self.add(parts.BattMeter(C(c, (-600.5,  68.75)), 'BA'))
		
		# radio-station
		self.add(parts.TRS(C(c, (-470, 175)), 'R'))
	# constructor
# class PanelPHL


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPHL(), True))
