# ***********************************************
# ***          Loco363 - Panel - PH           ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(4*'../' + 'lib/cad', __file__)
import generic


class PanelPH(generic.bases.LabelPanel2):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			'H',
			[
				(   0,   0),
				(1550,   0),
				(1550, 270),
				(   0, 270)
			]
		)
		
		c = self.getCoords()
		
		# sub-panels
		self.add(load('panels.PHL', '../l/_cad/label.py', __file__).PanelPHL(C(c, (  10, 10))))
		self.add(load('panels.PHS', '../s/_cad/label.py', __file__).PanelPHS(C(c, ( 670, 10))))
		self.add(load('panels.PHP', '../p/_cad/label.py', __file__).PanelPHP(C(c, ( 980, 10))))
		self.add(load('panels.PHZ', '../z/_cad/label.py', __file__).PanelPHZ(C(c, (1290, 10))))
	# constructor
# class PanelPH


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPH(), True))
