# ***********************************************
# ***         Loco363 - Panel - PHPS1         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load


# load other files
t6 = load('parts.t6', 6*'../' + 'parts/t6/cad/view.py', __file__)


class PHPS1(t6.T6_PilotLight):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 'ZÁVĚR', '#FD0')
	# constructor
# class PHPS1


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PHPS1(), True))
