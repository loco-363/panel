# ***********************************************
# ***          Loco363 - Panel - PHP          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(5*'../' + 'lib/cad', __file__)
import generic


class PanelPHP(generic.bases.ViewPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			[
				(  0,   0),
				(300,   0),
				(300, 250),
				(  0, 250)
			]
		)
		
		c = self.getCoords()
		
		# pilot-lights
		self.add(load('panel_parts.S1', '../s1/cad/view.py', __file__).PHPS1(C(c, (127, 68.75))))
		self.add(load('panel_parts.S2', '../s2/cad/view.py', __file__).PHPS2(C(c, (173, 68.75))))
	# constructor
# class PanelPHP


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPHP(), True))
