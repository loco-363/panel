# ***********************************************
# ***          Loco363 - Panel - PHP          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# path to panel GIT root
rootPath = 5 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic

# import parts
from types import SimpleNamespace
parts = SimpleNamespace()

parts.Meter = load('parts.meter', rootPath + 'parts/meter/cad/drill.py', __file__).Meter
parts.T6 = load('parts.t6', rootPath + 'parts/t6/cad/drill.py', __file__).T6


class PanelPHP(gen_draw.Drawing):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c)
		
		# make sure we pass a valid coordinates to our children nodes
		c = self.getCoords()
		
		# border
		self.add(generic.LabeledPolygon(
			c,
			[
				(  0,   0),
				(300,   0, (-2, -5, 'end')),
				(300, 250, (-2, -5, 'end')),
				(  0, 250)
			],
			{
				'fill': 'none',
				'stroke': 'black',
				'stroke-width': 3
			}
		))
		
		# mounting holes
		self.add(generic.DrillHole(C(c, ( 10,  10)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (150,  10)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (290,  10)), 4, 'Z', cpos=(-2, -5, 'end')))
		self.add(generic.DrillHole(C(c, (290, 240)), 4, 'Z', cpos=(-2, -5, 'end')))
		self.add(generic.DrillHole(C(c, (150, 240)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, ( 10, 240)), 4, 'Z'))
		
		# meters
		self.add(parts.Meter(C(c, (65,  68.75))))
		
		# indicators
		self.add(parts.T6(C(c, (127, 68.75))))
		self.add(parts.T6(C(c, (173, 68.75))))
	# constructor
# class PanelPHP


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPHP(), True))
