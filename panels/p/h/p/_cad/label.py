# ***********************************************
# ***          Loco363 - Panel - PHP          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# path to panel GIT root
rootPath = 5 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic

# import parts
from types import SimpleNamespace
parts = SimpleNamespace()

parts.Meter = load('parts.meter', rootPath + 'parts/meter/cad/label.py', __file__).Meter
parts.T6 = load('parts.t6', rootPath + 'parts/t6/cad/label.py', __file__).T6


class PanelPHP(generic.bases.LabelPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			'P',
			[
				(  0,   0),
				(300,   0),
				(300, 250),
				(  0, 250)
			],
			npos=(140, 10)
		)
		
		c = self.getCoords()
		
		# pilot-lights
		self.add(parts.T6(C(c, (127, 68.75)), 'S1'))
		self.add(parts.T6(C(c, (173, 68.75)), 'S2'))
		
		# meter
		self.add(parts.Meter(C(c, (65,  68.75)), 'PT'))
	# constructor
# class PanelPHP


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPHP(), True))
