# ***********************************************
# ***          Loco363 - Panel - PHZ          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

# path to panel GIT root
rootPath = 5 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic


class PanelPHZ(generic.bases.LabelPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			'Z',
			[
				(  0,   0),
				(250,   0),
				(250, 250),
				(  0, 250)
			]
		)
	# constructor
# class PanelPHZ


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPHZ(), True))
