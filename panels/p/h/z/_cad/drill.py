# ***********************************************
# ***          Loco363 - Panel - PHZ          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

# path to panel GIT root
rootPath = 5 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic


class PanelPHZ(gen_draw.Drawing):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c)
		
		# make sure we pass a valid coordinates to our children nodes
		c = self.getCoords()
		
		# border
		self.add(generic.LabeledPolygon(
			c,
			[
				(  0,   0),
				(250,   0, (-2, -5, 'end')),
				(250, 250, (-2, -5, 'end')),
				(  0, 250)
			],
			{
				'fill': 'none',
				'stroke': 'black',
				'stroke-width': 3
			}
		))
		
		# mounting holes
		self.add(generic.DrillHole(C(c, ( 10,  10)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (240,  10)), 4, 'Z', cpos=(-2, -5, 'end')))
		self.add(generic.DrillHole(C(c, (240, 240)), 4, 'Z', cpos=(-2, -5, 'end')))
		self.add(generic.DrillHole(C(c, ( 10, 240)), 4, 'Z'))
	# constructor
# class PanelPHZ


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPHZ(), True))
