# ***********************************************
# ***         Loco363 - Panel - PHSP          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load


# load other files
t6 = load('parts.t6', 6*'../' + 'parts/t6/cad/view.py', __file__)


class PHSP(t6.T6_PilotLight):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 'SIGNALIZACE\nPORUCH', '#C00')
	# constructor
# class PHSP


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PHSP(), True))
