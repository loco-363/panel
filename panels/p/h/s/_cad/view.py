# ***********************************************
# ***          Loco363 - Panel - PHS          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(5*'../' + 'lib/cad', __file__)
import generic


class PanelPHS(generic.bases.ViewPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			[
				(  0,   0),
				(300,   0),
				(300, 250),
				(  0, 250)
			]
		)
		
		c = self.getCoords()
		
		# pilot-light
		self.add(load('panel_parts.P', '../p/cad/view.py', __file__).PHSP(C(c, (255, 41))))
	# constructor
# class PanelPHS


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPHS(), True))
