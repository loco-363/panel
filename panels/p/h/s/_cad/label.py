# ***********************************************
# ***          Loco363 - Panel - PHS          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# path to panel GIT root
rootPath = 5 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic

# import parts
from types import SimpleNamespace
parts = SimpleNamespace()

parts.T6 = load('parts.t6', rootPath + 'parts/t6/cad/label.py', __file__).T6
parts.UCT_RJI16x = load('parts.uct_rji16x', rootPath + 'parts/uct-rji1/cad/label.py', __file__).UCT_RJI16x


class PanelPHS(generic.bases.LabelPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			'S',
			[
				(  0,   0),
				(300,   0),
				(300, 250),
				(  0, 250)
			]
		)
		
		c = self.getCoords()
		
		# speedometer
		self.add(parts.UCT_RJI16x(C(c, (150, 155)), 'R'))
		
		# pilot-light
		self.add(parts.T6(C(c, (255, 41)), 'P'))
		
		# lamp
		self.add(self._createLamp(C(c, (150, 47)), 'S'))
		self.add(self._createLampReg(C(c, (45, 41)), 'SR'))
		
		self.add(gen_draw.shapes.Text(
			C(c, (65, 51)),
			'SV',
			properties={
				'fill': self.COLOR_PART,
				'style': 'font-size:50pt; font-weight:bold;',
				'text-anchor': 'middle'
			}
		))
	# constructor
	
	def _createLamp(self, c, name):
		d = generic.bases.LabelPart(c, name)
		c = d.getCoords()
		
		# outline
		d.add(gen_draw.shapes.Rectangle(
			c,
			160,
			50,
			center=True,
			properties={
				'fill': d.COLOR_UNI,
				'stroke': d.color,
				'stroke-width': 3
			}
		))
		
		return d
	# _createLamp
	
	def _createLampReg(self, c, name):
		d = generic.bases.LabelPart(c, name)
		c = d.getCoords()
		
		# outline
		d.add(gen_draw.shapes.Circle(
			c,
			20,
			properties={
				'fill': d.COLOR_UNI,
				'stroke': d.color,
				'stroke-width': 3
			}
		))
		
		return d
	# _createLampReg
# class PanelPHS


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPHS(), True))
