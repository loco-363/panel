# ***********************************************
# ***           Loco363 - Panel - P           ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(3*'../' + 'lib/cad', __file__)
import generic


class PanelP(generic.bases.LabelPanel1):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			'P',
			[
				(   0, 100),
				(1130, 100),
				(1205,   0),
				(1570,   0),
				(1570, 720),
				(   0, 720)
			]
		)
		
		c = self.getCoords()
		
		# sub-panels
		self.add(load('panels.PD', '../d/_cad/label.py', __file__).PanelPD(C(c, (10,  10))))
		self.add(load('panels.PH', '../h/_cad/label.py', __file__).PanelPH(C(c, (10, 440))))
	# constructor
# class PanelP


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelP(), True))
