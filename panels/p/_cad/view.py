# ***********************************************
# ***           Loco363 - Panel - P           ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(3*'../' + 'lib/cad', __file__)
import generic


class PanelP(generic.bases.ViewPanel1):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			[
				(   0, 100),
				(1125, 100),
				(1200,   0),
				(1550,   0),
				(1550, 690),
				(   0, 690)
			]
		)
		
		c = self.getCoords()
		
		# sub-panels
		self.add(load('panels.PD', '../d/_cad/view.py', __file__).PanelPD(C(c, (0,   0))))
		self.add(load('panels.PH', '../h/_cad/view.py', __file__).PanelPH(C(c, (0, 420))))
	# constructor
# class PanelP


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelP(), True))
