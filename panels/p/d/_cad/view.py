# ***********************************************
# ***          Loco363 - Panel - PD           ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(4*'../' + 'lib/cad', __file__)
import generic


class PanelPD(generic.bases.ViewPanel2):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			[
				(   0, 100),
				(1125, 100),
				(1200,   0),
				(1550,   0),
				(1550, 420),
				(   0, 420)
			]
		)
		
		c = self.getCoords()
		
		# sub-panels
		self.add(load('panels.PDL', '../l/_cad/view.py', __file__).PanelPDL(C(c, (  10, 110))))
		self.add(load('panels.PDS', '../s/_cad/view.py', __file__).PanelPDS(C(c, ( 570, 110))))
		self.add(load('panels.PDP', '../p/_cad/view.py', __file__).PanelPDP(C(c, (1130,  10))))
		self.add(load('panels.PDB', '../b/_cad/view.py', __file__).PanelPDB(C(c, (1340,  10))))
	# constructor
# class PanelPD


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPD(), True))
