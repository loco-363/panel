/**
***********************************************
***         Loco363 - Panel - PDSKJ         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


use <../../../../../../parts/lever/3dp/housing.scad>;

include <common.scad>;


// compute 100 slices per full round
$fn=100;


intersection(){
	housing(pos=pos, mountH=-6);// mountH ~ 12mm for top cover and 6.3mm panel thickness
	
	union(){
		intersection(){
			translate([-20, 0, -1])
				cylinder(r=50, h=2+15+2);
			
			translate([6+6, -(1+80+1)/2, -1-1])
				cube([80+1, 1+80+1, 1+(2+15+2)+1]);
		}
		
		translate([-(80*2+1)+(6+6), -(1+110+1)/2, -1])
			cube([80*2+1, 1+110+1, 2+15+2]);
	}
}
