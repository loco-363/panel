/**
***********************************************
***         Loco363 - Panel - PDSKS         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


use <../../../../../../parts/lever/3dp/wheel.scad>;

include <common.scad>;


// compute 100 slices per full round
$fn=100;


difference(){
	for(i = [0 : 1])
		translate([0, i*(50+10), 0])
			rotate([i*180, 0, 0])
				wheel(pos=pos);
	
	translate([-100, -100, -100])
		cube([200, 200, 100]);
}
