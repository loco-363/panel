# ***********************************************
# ***         Loco363 - Panel - PDSKB         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load


# load other files
lever = load('parts.lever', 6*'../' + 'parts/lever/cad/view.py', __file__)


class PDSKB(lever.Lever):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, -1, ('+', 'B', '-'), size=(40, 50), pos_left=True)
	# constructor
# class PDSKB


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PDSKB(), True))
