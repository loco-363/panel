/**
***********************************************
***         Loco363 - Panel - PDSKB         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


use <../../../../../../parts/lever/3dp/housing.scad>;

include <common.scad>;


// compute 100 slices per full round
$fn=100;


intersection(){
	housing(pos=pos, mountH=-23);// mountH ~ 29mm for top cover and 6.3mm panel thickness
	
	union(){
		intersection(){
			translate([5, 0, -1])
				cylinder(r=35-3, h=2+15+2);
			
			translate([23+6, -(1+80+1)/2, -1-1])
				cube([80+1, 1+80+1, 1+(2+15+2)+1]);
		}
		
		translate([-(80*2+1)+(23+6), -(1+110+1)/2, -1])
			cube([80*2+1, 1+110+1, 2+15+2]);
	}
}
