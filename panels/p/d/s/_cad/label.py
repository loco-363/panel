# ***********************************************
# ***          Loco363 - Panel - PDS          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C
from gen_draw.shapes import Polygon

from path_loader import load

# path to panel GIT root
rootPath = 5 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic

# import parts
from types import SimpleNamespace
parts = SimpleNamespace()

parts.Lever = load('parts.lever', rootPath + 'parts/lever/cad/label.py', __file__).Lever
parts.T6 = load('parts.t6', rootPath + 'parts/t6/cad/label.py', __file__).T6


class PanelPDS(generic.bases.LabelPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			'S',
			[
				(  0,   0),
				(550,   0),
				(550, 300),
				(  0, 300)
			]
		)
		
		# move zero to (0, 300)
		c = C(self.getCoords(), (0, 300))
		
		# timetable
		self.add(Polygon(
			c,
			[
				(125, - 25),
				(375, - 25),
				(375, -210),
				(125, -210)
			],
			{
				'fill': 'none',
				'stroke': 'black',
				'stroke-linecap': 'round',
				'stroke-linejoin': 'round',
				'stroke-width': 3
			}
		))
		
		self.add(gen_draw.shapes.Line(
			C(c, (145, -222)),
			C(c, (355, -222)),
			{
				'fill': 'none',
				'stroke': '#CCC',
				'stroke-width': '16mm'
			}
		))
		
		# main controller levers
		self.add(parts.Lever(C(c, ( 95, -125)), 'KS'))
		self.add(parts.Lever(C(c, (405, -125)), 'KJ'))
		self.add(parts.Lever(C(c, (460, -125)), 'KB', size=(40, 50)))
		
		# dead-man's vigilance push-buttons
		self.add(parts.T6(C(c, ( 95, -250)), 'BL'))
		self.add(parts.T6(C(c, (455, -250)), 'BP'))
	# constructor
# class PanelPDS


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPDS(), True))
