# ***********************************************
# ***          Loco363 - Panel - PDS          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C
from gen_draw.shapes import Polygon

from path_loader import load

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(5*'../' + 'lib/cad', __file__)
import generic


class PanelPDS(generic.bases.ViewPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			[
				(  0,   0),
				(550,   0),
				(550, 300),
				(  0, 300)
			]
		)
		
		# move zero to (0, 300)
		c = C(self.getCoords(), (0, 300))
		
		# timetable
		self.add(Polygon(
			c,
			[
				(125, - 25),
				(375, - 25),
				(375, -210),
				(125, -210)
			],
			{
				'fill': 'none',
				'stroke': 'black',
				'stroke-linecap': 'round',
				'stroke-linejoin': 'round',
				'stroke-width': 3
			}
		))
		
		self.add(gen_draw.shapes.Line(
			C(c, (145, -222)),
			C(c, (355, -222)),
			{
				'fill': 'none',
				'stroke': '#CCC',
				'stroke-width': '16mm'
			}
		))
		
		# main controller levers
		self.add(load('panel_parts.KS', '../ks/cad/view.py', __file__).PDSKS(C(c, ( 95, -125))))
		self.add(load('panel_parts.KJ', '../kj/cad/view.py', __file__).PDSKJ(C(c, (405, -125))))
		self.add(load('panel_parts.KB', '../kb/cad/view.py', __file__).PDSKB(C(c, (460, -125))))
		
		# dead-man's vigilance push-buttons
		self.add(load('panel_parts.Bx', '../b_x/cad/view.py', __file__).PDSBx(C(c, ( 95, -250))))
		self.add(load('panel_parts.Bx', '../b_x/cad/view.py', __file__).PDSBx(C(c, (455, -250))))
	# constructor
# class PanelPDS


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPDS(), True))
