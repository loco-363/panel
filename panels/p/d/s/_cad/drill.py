# ***********************************************
# ***          Loco363 - Panel - PDS          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# path to panel GIT root
rootPath = 5 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic

# import parts
from types import SimpleNamespace
parts = SimpleNamespace()

parts.Lever = load('parts.lever', rootPath + 'parts/lever/cad/drill.py', __file__).Lever
parts.T6 = load('parts.t6', rootPath + 'parts/t6/cad/drill.py', __file__).T6


class PanelPDS(gen_draw.Drawing):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c)
		
		# make sure we pass a valid coordinates to our children nodes
		c = self.getCoords()
		
		# border
		self.add(generic.LabeledPolygon(
			c,
			[
				(  0,    0),
				(550,    0, (-2, -5, 'end')),
				(550, -300, (-2, -5, 'end')),
				(  0, -300)
			],
			{
				'fill': 'none',
				'stroke': 'black',
				'stroke-width': 3
			}
		))
		
		# mounting holes
		self.add(generic.DrillHole(C(c, ( 10, - 10)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (275, - 10)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (540, - 10)), 4, 'Z', cpos=(-2, -5, 'end')))
		self.add(generic.DrillHole(C(c, ( 10, -290)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (275, -290)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (540, -290)), 4, 'Z', cpos=(-2, -5, 'end')))
		
		# timetable
		self.add(generic.DrillHole(C(c, (125, - 25)), 4, 'RZ'))
		self.add(generic.DrillHole(C(c, (375, - 25)), 4, 'RZ'))
		self.add(generic.DrillHole(C(c, (125, -210)), 4, 'RZ'))
		self.add(generic.DrillHole(C(c, (375, -210)), 4, 'RZ'))
		
		self.add(generic.DrillHole(C(c, (160, -222)), 3))
		self.add(generic.DrillHole(C(c, (250, -222)), 3))
		self.add(generic.DrillHole(C(c, (340, -222)), 3))
		
		# main controller levers
		self.add(parts.Lever(C(c, ( 95, -125))))
		self.add(parts.Lever(C(c, (405, -125))))
		self.add(parts.Lever(C(c, (460, -125)), cutout=(36, 66)))
		
		# dead-man's vigilance push-buttons
		self.add(parts.T6(C(c, ( 95, -250))))
		self.add(parts.T6(C(c, (455, -250))))
	# constructor
# class PanelPDS


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPDS(), True))
