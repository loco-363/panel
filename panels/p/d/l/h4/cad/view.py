# ***********************************************
# ***         Loco363 - Panel - PDLH4         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load


# load other files
ms = load('parts.modswitch', 6*'../' + 'parts/modswitch/cad/view.py', __file__)


class PDLH4(ms.ModSwitch8):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 'TOPENÍ VLAKU', 7, ('1500V', '0', '3000V'), handle=False, hole=16, locks=(4,), pos_circle=True)
	# constructor
# class PDLH4


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PDLH4(), True))
