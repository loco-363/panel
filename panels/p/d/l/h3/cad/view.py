# ***********************************************
# ***         Loco363 - Panel - PDLH3         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load


# load other files
ms = load('parts.modswitch', 6*'../' + 'parts/modswitch/cad/view.py', __file__)


class PDLH3(ms.ModSwitch12):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 'NOUZOVÁ JÍZDA', 0, [0] + list(range(11)), label_both=True, handle=False, locks=(0,))
	# constructor
# class PDLH3


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PDLH3(), True))
