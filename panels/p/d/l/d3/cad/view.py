# ***********************************************
# ***         Loco363 - Panel - PDLD3         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load


# load other files
t6 = load('parts.t6', 6*'../' + 'parts/t6/cad/view.py', __file__)


class PDLD3(t6.T6_SwitchRot3Pos):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 'OSVĚTLENÍ\nMĚŘ. PŘÍSTROJŮ', '#000', pl=('TL', '0', 'PL'))
	# constructor
# class PDLD3


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PDLD3(), True))
