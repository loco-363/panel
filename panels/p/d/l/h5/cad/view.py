# ***********************************************
# ***         Loco363 - Panel - PDLH5         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load


# load other files
ms = load('parts.modswitch', 6*'../' + 'parts/modswitch/cad/view.py', __file__)


class PDLH5(ms.ModSwitch4):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 'ŘÍZENÍ', 0, ('0', 'I'), locks=(0,))
	# constructor
# class PDLH5


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PDLH5(), True))
