# ***********************************************
# ***          Loco363 - Panel - PDL          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(5*'../' + 'lib/cad', __file__)
import generic


class PanelPDL(generic.bases.ViewPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			[
				(  0,   0),
				(550,   0),
				(550, 300),
				(  0, 300)
			]
		)
		
		# move zero to (550, 300)
		c = C(self.getCoords(), (550, 300))
		
		# main switch off
		self.add(load('panel_parts.H1', '../h1/cad/view.py', __file__).PDLH1(C(c, (-56, -56))))
		
		# cabin light & backlights
		self.add(load('panel_parts.D2', '../d2/cad/view.py', __file__).PDLD2(C(c, (-142, -228))))
		self.add(load('panel_parts.D3', '../d3/cad/view.py', __file__).PDLD3(C(c, (-188, -228))))
		
		# signal lights
		self.add(load('panel_parts.D4', '../d4/cad/view.py', __file__).PDLD4(C(c, (-248, -228))))
		self.add(load('panel_parts.D5', '../d5/cad/view.py', __file__).PDLD5(C(c, (-294, -228))))
		self.add(load('panel_parts.D6', '../d6/cad/view.py', __file__).PDLD6(C(c, (-354, -228))))
		self.add(load('panel_parts.D7', '../d7/cad/view.py', __file__).PDLD7(C(c, (-400, -228))))
		
		# modular switches
		self.add(load('panel_parts.H2', '../h2/cad/view.py', __file__).PDLH2(C(c, (-142, -56))))
		self.add(load('panel_parts.H3', '../h3/cad/view.py', __file__).PDLH3(C(c, (-228, -56))))
		self.add(load('panel_parts.H4', '../h4/cad/view.py', __file__).PDLH4(C(c, (-314, -56))))
		self.add(load('panel_parts.H5', '../h5/cad/view.py', __file__).PDLH5(C(c, (-400, -56))))
		
		self.add(load('panel_parts.S1', '../s1/cad/view.py', __file__).PDLS1(C(c, (- 56, -142))))
		self.add(load('panel_parts.S2', '../s2/cad/view.py', __file__).PDLS2(C(c, (-142, -142))))
		self.add(load('panel_parts.S3', '../s3/cad/view.py', __file__).PDLS3(C(c, (-228, -142))))
		self.add(load('panel_parts.S4', '../s4/cad/view.py', __file__).PDLS4(C(c, (-314, -142))))
		
		self.add(load('panel_parts.D1', '../d1/cad/view.py', __file__).PDLD1(C(c, (-56, -228))))
	# constructor
# class PanelPDL


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPDL(), True))
