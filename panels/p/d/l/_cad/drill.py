# ***********************************************
# ***          Loco363 - Panel - PDL          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# path to panel GIT root
rootPath = 5 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic

# import parts
from types import SimpleNamespace
parts = SimpleNamespace()

parts.ModSwitch = load('parts.modswitch', rootPath + 'parts/modswitch/cad/drill.py', __file__).ModSwitch
parts.T6 = load('parts.t6', rootPath + 'parts/t6/cad/drill.py', __file__).T6


class PanelPDL(gen_draw.Drawing):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c)
		
		# make sure we pass a valid coordinates to our children nodes
		c = self.getCoords()
		
		# border
		self.add(generic.LabeledPolygon(
			c,
			[
				(   0,    0, (-2, -5, 'end')),
				(-550,    0),
				(-550, -300),
				(   0, -300, (-2, -5, 'end'))
			],
			{
				'fill': 'none',
				'stroke': 'black',
				'stroke-width': 3
			}
		))
		
		# mounting holes
		self.add(generic.DrillHole(C(c, (- 10, - 10)), 4, 'Z', cpos=(-2, -5, 'end')))
		self.add(generic.DrillHole(C(c, (-275, - 10)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (-540, - 10)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (- 10, -290)), 4, 'Z', cpos=(-2, -5, 'end')))
		self.add(generic.DrillHole(C(c, (-275, -290)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (-540, -290)), 4, 'Z'))
		
		# main switch off
		self.add(parts.T6(C(c, (-56, -56))))
		
		# cabin light & backlights
		self.add(parts.T6(C(c, (-142, -228))))
		self.add(parts.T6(C(c, (-188, -228))))
		
		# signal lights
		self.add(parts.T6(C(c, (-248, -228))))
		self.add(parts.T6(C(c, (-294, -228))))
		self.add(parts.T6(C(c, (-354, -228))))
		self.add(parts.T6(C(c, (-400, -228))))
		
		# modular switches
		self.add(parts.ModSwitch(C(c, (-142, -56))))
		self.add(parts.ModSwitch(C(c, (-228, -56))))
		self.add(parts.ModSwitch(C(c, (-314, -56))))
		self.add(parts.ModSwitch(C(c, (-400, -56))))
		
		self.add(parts.ModSwitch(C(c, (- 56, -142))))
		self.add(parts.ModSwitch(C(c, (-142, -142))))
		self.add(parts.ModSwitch(C(c, (-228, -142))))
		self.add(parts.ModSwitch(C(c, (-314, -142))))
		
		self.add(parts.ModSwitch(C(c, (-56, -228))))
	# constructor
# class PanelPDL


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPDL(), True))
