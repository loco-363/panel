# ***********************************************
# ***          Loco363 - Panel - PDL          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# path to panel GIT root
rootPath = 5 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic

# import parts
from types import SimpleNamespace
parts = SimpleNamespace()

parts.ModSwitch = load('parts.modswitch', rootPath + 'parts/modswitch/cad/label.py', __file__).ModSwitch
parts.T6 = load('parts.t6', rootPath + 'parts/t6/cad/label.py', __file__).T6


class PanelPDL(generic.bases.LabelPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			'L',
			[
				(  0,   0),
				(550,   0),
				(550, 300),
				(  0, 300)
			]
		)
		
		# move zero to (550, 300)
		c = C(self.getCoords(), (550, 300))
		
		# main switch off
		self.add(parts.T6(C(c, (-56, -56)), 'H1'))
		
		# cabin light & backlights
		self.add(parts.T6(C(c, (-142, -228)), 'D2'))
		self.add(parts.T6(C(c, (-188, -228)), 'D3'))
		
		# signal lights
		self.add(parts.T6(C(c, (-248, -228)), 'D4'))
		self.add(parts.T6(C(c, (-294, -228)), 'D5'))
		self.add(parts.T6(C(c, (-354, -228)), 'D6'))
		self.add(parts.T6(C(c, (-400, -228)), 'D7'))
		
		# modular switches
		self.add(parts.ModSwitch(C(c, (-142, -56)), 'H2'))
		self.add(parts.ModSwitch(C(c, (-228, -56)), 'H3'))
		self.add(parts.ModSwitch(C(c, (-314, -56)), 'H4'))
		self.add(parts.ModSwitch(C(c, (-400, -56)), 'H5'))
		
		self.add(parts.ModSwitch(C(c, (- 56, -142)), 'S1'))
		self.add(parts.ModSwitch(C(c, (-142, -142)), 'S2'))
		self.add(parts.ModSwitch(C(c, (-228, -142)), 'S3'))
		self.add(parts.ModSwitch(C(c, (-314, -142)), 'S4'))
		
		self.add(parts.ModSwitch(C(c, (-56, -228)), 'D1'))
	# constructor
# class PanelPDL


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPDL(), True))
