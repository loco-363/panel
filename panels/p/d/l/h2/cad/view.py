# ***********************************************
# ***         Loco363 - Panel - PDLH2         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load


# load other files
ms = load('parts.modswitch', 6*'../' + 'parts/modswitch/cad/view.py', __file__)


class PDLH2(ms.ModSwitch8):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 'PŘEP. SYST. + HV', 6, ('ZAP.', '=', '0', '~', 'ZAP.'))
	# constructor
# class PDLH2


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PDLH2(), True))
