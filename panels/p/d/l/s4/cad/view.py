# ***********************************************
# ***         Loco363 - Panel - PDLS4         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load


# load other files
ms = load('parts.modswitch', 6*'../' + 'parts/modswitch/cad/view.py', __file__)


class PDLS4(ms.ModSwitch8):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 'VENTILÁTORY', 7, ('VYP.', 'A', 'R'))
	# constructor
# class PDLS4


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PDLS4(), True))
