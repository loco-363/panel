# ***********************************************
# ***         Loco363 - Panel - PDPH2         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load


# load other files
ms = load('parts.modswitch', 6*'../' + 'parts/modswitch/cad/view.py', __file__)


class PDPH2(ms.ModSwitch8):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 'REŽIM JÍZDY', 6, ('P', 'V', 'J', 'S'))
	# constructor
# class PDPH2


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PDPH2(), True))
