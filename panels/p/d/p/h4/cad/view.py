# ***********************************************
# ***         Loco363 - Panel - PDPH4         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load


# load other files
t6 = load('parts.t6', 6*'../' + 'parts/t6/cad/view.py', __file__)


class PDPH4(t6.T6_SwitchRot2Pos):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 'FREKVENCE VZ', '#000', pl=('50Hz', '75Hz'))
	# constructor
# class PDPH4


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PDPH4(), True))
