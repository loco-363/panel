# ***********************************************
# ***         Loco363 - Panel - PDPH3         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load


# load other files
ms = load('parts.modswitch', 6*'../' + 'parts/modswitch/cad/view.py', __file__)


class PDPH3(ms.ModSwitch12):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 'POMĚRNÝ TAH', 0, range(1, 13), label_both=True)
	# constructor
# class PDPH3


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PDPH3(), True))
