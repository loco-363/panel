# ***********************************************
# ***         Loco363 - Panel - PDPD5         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load


# load other files
t6 = load('parts.t6', 6*'../' + 'parts/t6/cad/view.py', __file__)


class PDPD5(t6.T6_PushButton):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 'ODBRZĎOVAČ', '#000', cap=t6.T6_PushButton_Cap.Ergo)
	# constructor
# class PDPD5


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PDPD5(), True))
