# ***********************************************
# ***          Loco363 - Panel - PDP          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(5*'../' + 'lib/cad', __file__)
import generic


class PanelPDP(generic.bases.ViewPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			[
				(  0, 100),
				( 75,   0),
				(200,   0),
				(200, 400),
				(  0, 400)
			]
		)
		
		# move zero to (0, 400)
		c = C(self.getCoords(), (0, 400))
		
		# modular switches
		self.add(load('panel_parts.H1', '../h1/cad/view.py', __file__).PDPH1(C(c, ( 57, - 54))))
		self.add(load('panel_parts.H2', '../h2/cad/view.py', __file__).PDPH2(C(c, ( 57, -127))))
		self.add(load('panel_parts.H3', '../h3/cad/view.py', __file__).PDPH3(C(c, (143, -127))))
		
		# frequency of train protection system
		self.add(load('panel_parts.H4', '../h4/cad/view.py', __file__).PDPH4(C(c, (143, -54))))
		
		# speeds keypad
		self.add(load('panel_parts.K', '../../../../../parts/keypad/cad/view.py', __file__).Keypad(C(c, (100, -209))))
		
		# lower switches and buttons
		self.add(load('panel_parts.D1', '../d1/cad/view.py', __file__).PDPD1(C(c, ( 54, -289.5))))
		self.add(load('panel_parts.D2', '../d2/cad/view.py', __file__).PDPD2(C(c, (100, -289.5))))
		self.add(load('panel_parts.D3', '../d3/cad/view.py', __file__).PDPD3(C(c, (146, -289.5))))
		self.add(load('panel_parts.D4', '../d4/cad/view.py', __file__).PDPD4(C(c, (100, -346.5))))
		self.add(load('panel_parts.D5', '../d5/cad/view.py', __file__).PDPD5(C(c, (146, -346.5))))
	# constructor
# class PanelPDP


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPDP(), True))
