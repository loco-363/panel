# ***********************************************
# ***          Loco363 - Panel - PDP          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# path to panel GIT root
rootPath = 5 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic

# import parts
from types import SimpleNamespace
parts = SimpleNamespace()

parts.Keypad = load('parts.keypad', rootPath + 'parts/keypad/cad/label.py', __file__).Keypad
parts.ModSwitch = load('parts.modswitch', rootPath + 'parts/modswitch/cad/label.py', __file__).ModSwitch
parts.T6 = load('parts.t6', rootPath + 'parts/t6/cad/label.py', __file__).T6


class PanelPDP(generic.bases.LabelPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			'P',
			[
				(  0, 100),
				( 75,   0),
				(200,   0),
				(200, 400),
				(  0, 400)
			],
			npos=(45, 50)
		)
		
		# move zero to (0, 400)
		c = C(self.getCoords(), (0, 400))
		
		# modular switches
		self.add(parts.ModSwitch(C(c, ( 57, - 54)), 'H1'))
		self.add(parts.ModSwitch(C(c, ( 57, -127)), 'H2'))
		self.add(parts.ModSwitch(C(c, (143, -127)), 'H3'))
		
		# frequency of train protection system
		self.add(parts.T6(C(c, (143, -54)), 'H4'))
		
		# speeds keypad
		self.add(parts.Keypad(C(c, (100, -209)), 'K'))
		
		# lower switches and buttons
		self.add(parts.T6(C(c, ( 54, -289.5)), 'D1'))
		self.add(parts.T6(C(c, (100, -289.5)), 'D2'))
		self.add(parts.T6(C(c, (146, -289.5)), 'D3'))
		self.add(parts.T6(C(c, (100, -346.5)), 'D4'))
		self.add(parts.T6(C(c, (146, -346.5)), 'D5'))
	# constructor
# class PanelPDP


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPDP(), True))
