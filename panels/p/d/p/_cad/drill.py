# ***********************************************
# ***          Loco363 - Panel - PDP          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# path to panel GIT root
rootPath = 5 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic

# import parts
from types import SimpleNamespace
parts = SimpleNamespace()

parts.Keypad = load('parts.keypad', rootPath + 'parts/keypad/cad/drill.py', __file__).Keypad
parts.ModSwitch = load('parts.modswitch', rootPath + 'parts/modswitch/cad/drill.py', __file__).ModSwitch
parts.T6 = load('parts.t6', rootPath + 'parts/t6/cad/drill.py', __file__).T6


class PanelPDP(gen_draw.Drawing):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c)
		
		# make sure we pass a valid coordinates to our children nodes
		c = self.getCoords()
		
		# border
		self.add(generic.LabeledPolygon(
			c,
			[
				(  0,    0),
				(200,    0, (-2, -5, 'end')),
				(200, -400, (-2, -5, 'end')),
				( 75, -400),
				(  0, -300)
			],
			{
				'fill': 'none',
				'stroke': 'black',
				'stroke-width': 3
			}
		))
		
		# modular switches
		self.add(parts.ModSwitch(C(c, ( 57, - 54))))
		self.add(parts.ModSwitch(C(c, ( 57, -127))))
		self.add(parts.ModSwitch(C(c, (143, -127))))
		
		# mounting holes
		self.add(generic.DrillHole(C(c, ( 10, - 10)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (190, - 10)), 4, 'Z', cpos=(-2, -5, 'end')))
		self.add(generic.DrillHole(C(c, ( 10, -290)), 4, 'Z', cpos=(-5, -6)))
		self.add(generic.DrillHole(C(c, ( 85, -390)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (190, -390)), 4, 'Z', cpos=(-2, -5, 'end')))
		
		# frequency of train protection system
		self.add(parts.T6(C(c, (143, -54))))
		
		# speeds keypad
		self.add(parts.Keypad(C(c, (100, -209))))
		
		# lower switches and buttons
		self.add(parts.T6(C(c, ( 54, -289.5))))
		self.add(parts.T6(C(c, (100, -289.5))))
		self.add(parts.T6(C(c, (146, -289.5))))
		self.add(parts.T6(C(c, (100, -346.5))))
		self.add(parts.T6(C(c, (146, -346.5))))
	# constructor
# class PanelPDP


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPDP(), True))
