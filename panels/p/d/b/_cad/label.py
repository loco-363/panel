# ***********************************************
# ***          Loco363 - Panel - PDB          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# path to panel GIT root
rootPath = 5 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic

# import parts
from types import SimpleNamespace
parts = SimpleNamespace()

parts.T6 = load('parts.t6', rootPath + 'parts/t6/cad/label.py', __file__).T6


class PanelPDB(generic.bases.LabelPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			'B',
			[
				(  0,   0),
				(200,   0),
				(200, 400),
				(  0, 400)
			]
		)
		
		# move zero to (0, 400)
		c = C(self.getCoords(), (0, 400))
		
		# coupling control
		self.add(parts.T6(C(c, (167.5, -200)), 'P1'))
		
		# relative traction manipulation
		self.add(parts.T6(C(c, (167.5, -277)), 'P2'))
		self.add(parts.T6(C(c, (167.5, -334)), 'P3'))
	# constructor
# class PanelPDB


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPDB(), True))
