# ***********************************************
# ***          Loco363 - Panel - PDB          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# path to panel GIT root
rootPath = 5 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic

# import parts
from types import SimpleNamespace
parts = SimpleNamespace()

parts.T6 = load('parts.t6', rootPath + 'parts/t6/cad/drill.py', __file__).T6


class PanelPDB(gen_draw.Drawing):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c)
		
		# make sure we pass a valid coordinates to our children nodes
		c = self.getCoords()
		
		# border
		self.add(generic.LabeledPolygon(
			c,
			[
				(  0,    0),
				(200,    0, (-2, -5, 'end')),
				(200, -400, (-2, -5, 'end')),
				(  0, -400)
			],
			{
				'fill': 'none',
				'stroke': 'black',
				'stroke-width': 3
			}
		))
		
		# mounting holes
		self.add(generic.DrillHole(C(c, ( 10, - 10)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (190, - 10)), 4, 'Z', cpos = (-2, -5, 'end')))
		self.add(generic.DrillHole(C(c, ( 10, -390)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (190, -390)), 4, 'Z', cpos = (-2, -5, 'end')))
		
		# coupling control
		self.add(parts.T6(C(c, (167.5, -200))))
		
		# relative traction manipulation
		self.add(parts.T6(C(c, (167.5, -277))))
		self.add(parts.T6(C(c, (167.5, -334))))
	# constructor
# class PanelPDB


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPDB(), True))
