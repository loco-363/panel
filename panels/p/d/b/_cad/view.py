# ***********************************************
# ***          Loco363 - Panel - PDB          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(5*'../' + 'lib/cad', __file__)
import generic


class PanelPDB(generic.bases.ViewPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			[
				(  0,   0),
				(200,   0),
				(200, 400),
				(  0, 400)
			]
		)
		
		# move zero to (0, 400)
		c = C(self.getCoords(), (0, 400))
		
		# coupling control
		self.add(load('panel_parts.P1', '../p1/cad/view.py', __file__).PDBP1(C(c, (167.5, -200))))
		
		# relative traction manipulation
		self.add(load('panel_parts.P2', '../p2/cad/view.py', __file__).PDBP2(C(c, (167.5, -277))))
		self.add(load('panel_parts.P3', '../p3/cad/view.py', __file__).PDBP3(C(c, (167.5, -334))))
	# constructor
# class PanelPDB


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelPDB(), True))
