# ***********************************************
# ***             Loco363 - Panel             ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(2*'../' + 'lib/cad', __file__)
import generic


class Panel(generic.bases.Label):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - Coords of P panel bottom-left corner
		'''
		
		super().__init__(c)
		c = self.getCoords()
		
		# sub-panels
		self.add(load('panels.P', '../p/_cad/label.py', __file__).PanelP(C(c, (0,   10))))
		self.add(load('panels.D', '../d/_cad/label.py', __file__).PanelD(C(c, (0, -560))))
	# constructor
# class Panel


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(Panel(), True))
