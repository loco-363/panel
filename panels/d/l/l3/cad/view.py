# ***********************************************
# ***         Loco363 - Panel - DLL3          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load


# load other files
ms = load('parts.modswitch', 5*'../' + 'parts/modswitch/cad/view.py', __file__)


class DLL3(ms.ModSwitch4):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 'DIAGNOSTIKA-JÍZDA', 0, ('J', 'NJ', 'ZB', 'ZK'), label_both=True, locks=(1, 2, 3))
	# constructor
# class DLL3


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(DLL3(), True))
