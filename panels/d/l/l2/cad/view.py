# ***********************************************
# ***         Loco363 - Panel - DLL2          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load


# load other files
ms = load('parts.modswitch', 5*'../' + 'parts/modswitch/cad/view.py', __file__)


class DLL2(ms.ModSwitch8):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 'PORUCHA MOT. SKUPINY', 7, ('PM1', 'J', 'PM2'))
	# constructor
# class DLL2


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(DLL2(), True))
