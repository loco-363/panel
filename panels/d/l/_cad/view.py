# ***********************************************
# ***          Loco363 - Panel - DL           ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(4*'../' + 'lib/cad', __file__)
import generic


class PanelDL(generic.bases.ViewPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			[
				(  0,   0),
				(200,   0),
				(200, 400),
				(  0, 400)
			]
		)
		
		# move zero to (200, 400)
		c = C(self.getCoords(), (200, 400))
		
		# left column
		self.add(load('panel_parts.L1', '../l1/cad/view.py', __file__).DLL1(C(c, (-143, - 57))))
		self.add(load('panel_parts.L2', '../l2/cad/view.py', __file__).DLL2(C(c, (-143, -143))))
		self.add(load('panel_parts.L3', '../l3/cad/view.py', __file__).DLL3(C(c, (-143, -229))))
		
		# right column
		self.add(load('panel_parts.P1', '../p1/cad/view.py', __file__).DLP1(C(c, (-57, - 57))))
		self.add(load('panel_parts.P2', '../p2/cad/view.py', __file__).DLP2(C(c, (-57, -143))))
		self.add(load('panel_parts.P3', '../p3/cad/view.py', __file__).DLP3(C(c, (-57, -229))))
		self.add(load('panel_parts.P4', '../p4/cad/view.py', __file__).DLP4(C(c, (-57, -315))))
	# constructor
# class PanelDL


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelDL(), True))
