# ***********************************************
# ***          Loco363 - Panel - DL           ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# path to panel GIT root
rootPath = 4 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic

# import parts
from types import SimpleNamespace
parts = SimpleNamespace()

parts.ModSwitch = load('parts.modswitch', rootPath + 'parts/modswitch/cad/drill.py', __file__).ModSwitch
parts.T6 = load('parts.t6', rootPath + 'parts/t6/cad/drill.py', __file__).T6


class PanelDL(gen_draw.Drawing):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c)
		
		# make sure we pass a valid coordinates to our children nodes
		c = self.getCoords()
		
		# border
		self.add(generic.LabeledPolygon(
			c,
			[
				(   0,    0, (-2, -5, 'end')),
				(-200,    0),
				(-200, -400),
				(   0, -400, (-2, -5, 'end'))
			],
			{
				'fill': 'none',
				'stroke': 'black',
				'stroke-width': 3
			}
		))
		
		# mounting holes
		self.add(generic.DrillHole(C(c, (- 10, - 10)), 4, 'Z', cpos=(-2, -5, 'end')))
		self.add(generic.DrillHole(C(c, (-190, - 10)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (-190, -200)), 4, 'Z', cpos=(0, 6)))
		self.add(generic.DrillHole(C(c, (-190, -390)), 4, 'Z'))
		self.add(generic.DrillHole(C(c, (- 10, -390)), 4, 'Z', cpos=(-2, -5, 'end')))
		self.add(generic.DrillHole(C(c, (- 10, -200)), 4, 'Z', cpos=(0, 4, 'end')))
		
		# left column
		self.add(parts.T6(C(c, (-143, -57))))
		self.add(parts.ModSwitch(C(c, (-143, -143))))
		self.add(parts.ModSwitch(C(c, (-143, -229))))
		
		# right column
		self.add(parts.ModSwitch(C(c, (-57, - 57))))
		self.add(parts.ModSwitch(C(c, (-57, -143))))
		self.add(parts.ModSwitch(C(c, (-57, -229))))
		self.add(parts.ModSwitch(C(c, (-57, -315))))
	# constructor
# class PanelDL


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelDL(), True))
