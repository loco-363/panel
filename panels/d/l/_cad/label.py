# ***********************************************
# ***          Loco363 - Panel - DL           ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# path to panel GIT root
rootPath = 4 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic

# import parts
from types import SimpleNamespace
parts = SimpleNamespace()

parts.ModSwitch = load('parts.modswitch', rootPath + 'parts/modswitch/cad/label.py', __file__).ModSwitch
parts.T6 = load('parts.t6', rootPath + 'parts/t6/cad/label.py', __file__).T6


class PanelDL(generic.bases.LabelPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			'L',
			[
				(  0,   0),
				(200,   0),
				(200, 400),
				(  0, 400)
			]
		)
		
		# move zero to (200, 400)
		c = C(self.getCoords(), (200, 400))
		
		# left column
		self.add(parts.T6(C(c, (-143, -57)), 'L1'))
		self.add(parts.ModSwitch(C(c, (-143, -143)), 'L2'))
		self.add(parts.ModSwitch(C(c, (-143, -229)), 'L3'))
		
		# right column
		self.add(parts.ModSwitch(C(c, (-57, - 57)), 'P1'))
		self.add(parts.ModSwitch(C(c, (-57, -143)), 'P2'))
		self.add(parts.ModSwitch(C(c, (-57, -229)), 'P3'))
		self.add(parts.ModSwitch(C(c, (-57, -315)), 'P4'))
	# constructor
# class PanelDL


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelDL(), True))
