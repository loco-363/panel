# ***********************************************
# ***         Loco363 - Panel - DLP1          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load


# load other files
ms = load('parts.modswitch', 5*'../' + 'parts/modswitch/cad/view.py', __file__)


class DLP1(ms.ModSwitch8):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c, 'OSTŘIK. STĚRAČE', 6, ('I\nOS', 'I', '0', 'II', 'II\nOS'))
	# constructor
# class DLP1


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(DLP1(), True))
