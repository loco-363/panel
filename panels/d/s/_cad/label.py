# ***********************************************
# ***          Loco363 - Panel - DS           ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# path to panel GIT root
rootPath = 4 * '../'

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(rootPath + 'lib/cad', __file__)
import generic


class PanelDS(generic.bases.LabelPanel3):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			'S',
			[
				(  0,   0),
				(550,   0),
				(550, 100),
				(  0, 100)
			]
		)
		
		c = self.getCoords()
		
		# pedals
		self.add(self._createPedal(C(c, ( 70, 50)), 'L'))
		self.add(self._createPedal(C(c, (480, 50)), 'P'))
	# constructor
	
	def _createPedal(self, c, name):
		d = generic.bases.LabelPart(c, name)
		
		d.add(gen_draw.shapes.Rectangle(
			d.getCoords(),
			40,
			80,
			center=True,
			properties={
				'fill': d.COLOR_UNI,
				'stroke': d.color,
				'stroke-width': 3
			}
		))
		
		return d
# class PanelDS


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelDS(), True))
