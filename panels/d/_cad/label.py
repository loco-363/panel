# ***********************************************
# ***           Loco363 - Panel - D           ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

from path_loader import load

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(3*'../' + 'lib/cad', __file__)
import generic


class PanelD(generic.bases.LabelPanel1):
	def __init__(self, c = None):
		'''Constructor
		
		@param c - bottom-left corner Coords
		'''
		
		super().__init__(
			c,
			'D',
			[
				(   0,   0),
				(1140,   0),
				(1140, 550),
				(   0, 550)
			]
		)
		
		c = self.getCoords()
		
		# properties of lines outlining whole real panels
		props = {
			'fill': 'none',
			'stroke': self.COLOR_PANEL_3,
			'stroke-dasharray': '20,20',
			'stroke-linecap': 'square',
			'stroke-linejoin': 'square',
			'stroke-width': 6
		}
		
		# sub-panels
		self.add(load('panels.DL', '../l/_cad/label.py', __file__).PanelDL(C(c, (300, 140))))
		self.addBefore(gen_draw.shapes.Rectangle(
			C(c, (10, 10)),
			560,
			530,
			properties=props
		))
		
		self.add(load('panels.DS', '../s/_cad/label.py', __file__).PanelDS(C(c, (580,  10))))
		self.addBefore(gen_draw.shapes.Rectangle(
			C(c, (580, 10)),
			550,
			530,
			properties=props
		))
	# constructor
# class PanelD


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(PanelD(), True))
