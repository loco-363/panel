Loco363 - Panel
===============

Control Panel of Locomotive 363 (Škoda 69E)

This repo holds the specific variants and modifications of more generic parts (submoduled from separate repos).

CAD
---
Requires Python modules:
- GenDraw <https://gitlab.com/elektro-potkan/gen-draw>
- PathLoader <https://gitlab.com/elektro-potkan/path-loader>

License
-------
This program is licensed under the GNU General Public License version 3 or (at your option) any later version (GNU GPLv3+).

See file [LICENSE](LICENSE).

> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
> 
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
> 
> You should have received a copy of the GNU General Public License
> along with this program.  If not, see <http://www.gnu.org/licenses/>.
