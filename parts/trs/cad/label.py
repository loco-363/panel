# ***********************************************
# ***          Loco363 - Parts - TRS          ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert(3*'../' + 'lib/cad', __file__)
import generic


class TRS(generic.bases.LabelPart):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		c = self.getCoords()
		
		# outline
		self.add(gen_draw.shapes.Rectangle(
			c,
			320,
			130,
			center=True,
			properties={
				'fill': self.COLOR_UNI,
				'stroke': self.color,
				'stroke-dasharray': '3,3',
				'stroke-width': 3
			}
		))
		
		# inner area
		self.add(gen_draw.shapes.Rectangle(
			c,
			300,
			110,
			center=True,
			properties={
				'fill': 'white'
			}
		))
	# constructor
# class TRS


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(TRS(name='TRS'), True))
